## ROADTRIP PODCASTS


Mix of true crime and other stuff

### 1. Oi 99% Invisible

Genre: Life, Explained

What it’s all about: 99% Invisible is all about the design details we never think about—how people record sound at sporting events, the elements that make McMansions so awful or the placement of missing kids’ pictures on the back of milk cartons. It may sound random, but by the time each episode is over, these explorations illustrate larger truths about the world around us.

Perfect for: People who sweat the small stuff

Episode to get hooked on: “Milk Carton Kids”


### 2. Homecoming

**Genre:** Fiction, Mystery

**What it’s all about:** The scripted psychological thriller opens on a meeting with a soldier who has just returned home and slowly reveals its mystery from there. It boasts voice acting by Oscar Isaac, Catherine Keener and **David Schwimmer** (ROSS FUCKING GELLER JOAN!), among other stars.

**Perfect for:** People who loved The Night Of and Gone Girl


### 3. My Dad wrote a Porno

**Genre:** Comedy

**What it’s all about:** Host Jamie Morton’s father went through a mid-life crisis and wrote an erotic novel called “Belinda Blinked.” It’s bad. Very bad. Even the title is confusing: Did he mix up blinked with winked? Most people would be mortified, but Morton and his friends read and mercilessly lampoon a new chapter every week.

**Perfect for:** People who aren’t easily embarrassed by their parents’ Facebook pages

**Episode to get hooked on:** “S1E1 - The Job Interview”



### 4. Criminal

**Genre: Crime**  

True crime stories of people who’ve done wrong, been wronged, or gotten caught somewhere in the middle.



### 5.  The Memory Palace

Hardcore history built on small moments.  
Nate DiMeo’s podcast distils history into brief moving tales of past lives, aiming to pack the same emotional punch as a three-minute pop song. In host and creator Nate DiMeo’s hands every aspect of history becomes a fascinating exploration of human nature, ambition, fortitude and, occasionally, goats.  
This podcast is used in interactive museum exhibits that are meant to be pretty damn moving.  
99% Invisible’s Roman Mars described The Memory Palace as “sometimes heartbreaking, sometimes hysterical, and often a wonderful mix of both.”

### 6. True Murder

Each week author Dan Zupansky interviews fellow crime writers about their books about the most famous killers of all time such Ted Bundy and John Wayne Gacy.


### 7. My favourite murder

Hosts Georgia Hardstark and Karen Kilgariff will be the first to tell you that My Favorite Murder is filed under the “comedy” section for a reason. They don’t care too much about facts, but they do tell a great story. These are just two L.A.-based comedians with anxiety issues and a love for true crime who wanted to record their off-color banter and see what happened. They ended up with a hit podcast – number one on the Comedy charts six months after its release – and a growing legion of very dedicated fans. Kilgariff, a veteran of cult HBO hit Mr. Show, and Hardstark, one half of the Cooking Channel’s BFF road trip show Tripping out with Alie & Georgia, get together weekly to talk murder. Each comes prepared with a story of a murder to share with the other, but they fill the rest of the hour-plus episodes with listener contributions of hometown murders, hilarious commentary, and on-point true crime recommendations.

### 8. Actual innocence
**THIS ONE SOUNDS CLASS**  
We were hooked after one episode of Actual Innocence—which is frustrating because only five have been produced so far and they’re all damn addictive. Each follows a person who served time for a crime they did not commit and was later exonerated, which makes for fascinating, heartbreaking audio. Actual Innocence doesn’t have the same production quality as Serial, but it’s worth listening to regardless.  
For those who thought the best part of Serial was hearing a person describe the pains of being unjustly imprisoned for years, Actual Innocence is the podcast for you. Started last April by social worker Brooke Gittings, Actual Innocence was borne out of an innocence of Gittings’ own: It wasn’t until she listened to the seminal NPR podcast and watched Netflix’s hit Making a Murderer that she realized false imprisonment was such a big problem. “I didn’t realize that people actually got wrongfully convicted,” Gittings explains. “So I decided to make the show. I don’t think that the problem can be changed if people aren’t aware that it exists.” In the harrowing podcasts, which last about 50 minutes, Gittings interviews a person who have seen the worst parts of the criminal justice system and lived to tell their tales.

### 9. Casefile

As Casefile points out in their tagline, fact is scarier than fiction. But what the podcast might really prove is that fact is even scarier when told in a thick Australian accent – especially when accompanied by ambient, pulsing noise from a trio of professional sound designers and musicians. In each weekly episode, which can run anywhere from 20 minutes to an hour and a half, the narrator, known only as “Brad,” calmly tells a story of a devastating Australian crime. The podcast expertly covers murder and abduction, sometimes walking the listener through the criminal’s trial, and other times discussing potential theories for a crime whose perpetrator was never caught.

### 10. Someone Knows Something

Like Serial, Someone Knows Something follows one case over multiple episodes. This season (the first) the program is taking a deep dive into the disappearance of a five-year-old boy in eastern Ontario back in 1972.

### 11. Unsolved

Unsolved is a seven-part podcast investigating the murder of 14-year-old John Zera, who was mysteriously killed back in 1976\. Host and reporter Gina Barton digs through old evidence and speaks with people originally involved with trying to solve the case.

